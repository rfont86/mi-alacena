const butonTemplate = document.createElement('template')
butonTemplate.innerHTML = `
  <style>
    .container {
      width: 500px;
      margin: 0 auto;
    }

    input {
      padding: 12px 20px;
      margin: 8px 0;
      font-size: 16px;
      font-weight: bold;
      width: 50%;
      box-sizing: border-box;
      display: inline;
    }
  
    button {
      overflow: hidden;
      padding: 0 16px;
      font-size: 16px;
      font-weight: bold;
      outline: none;
      height: 46px;
      border: 1px solid #a1a1a1;
      background: #ffffff;
      box-shadow: 0 2px 4px 0 rgba(0,0,0, 0.05), 0 2px 8px 0 rgba(161,161,161, 0.4);
      color: #363636;
      cursor: pointer;
      display: inline;
    }
  </style>

  <div class="container">
    <h1>Alacena Killers <br></h1>
    <h2>La compra inteligente </h2>
    <h2>AÑADE UN PRODUCTO</h2>
    <br>
    <label> Añadir producto:</label><br>
    <input type="text" id="fname" name="fname" placeholder="ejem: patatas 1 kg">
    <button>Añadir</button>
    <ul></ul>
  </div>
`

class Button extends HTMLElement {
  constructor() {
    super()
    this._shadowRoot = this.attachShadow({ mode: 'open' })
    this._shadowRoot.appendChild(butonTemplate.content.cloneNode(true))

    this.unorderedList = this._shadowRoot.querySelector('ul')

    this.productList = []
  }

  setItems(value){
    this.setAttribute('items', value)
    value.forEach(x => this.productList.push(x))
    
    this.render()
  }
  
  render() {
    this.productList.forEach(item => {
      let listLi = document.createElement('li')
      listLi.innerHTML = item
      this.unorderedList.appendChild(listLi)
    })

    this.productList = []  
  }
}

export default Button
