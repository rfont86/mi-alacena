class ApiClient{

  getJson(route, callback) {
    this.api_url = "http://localhost:8081" + route

    fetch(this.api_url)
      .then(response => response.json())
      .then(data => callback(data))
  }
}

export default ApiClient