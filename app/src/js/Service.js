class Service {

  constructor(bus, apiClient){
    this.bus = bus
    this.apiClient = apiClient

    this._subscribe()
  }

  _subscribe(){
    this.bus.subscribe('obtain.data', this._retrieveInitialData.bind(this))
    this.bus.subscribe('add.data', this._addProduct.bind(this))
  }

  _buildCallback(message) {
    return (response) => {
      this.bus.publish(message, response)
    }
  }

  _retrieveInitialData(){
    const callback = this._buildCallback('obtained.data')
    this.apiClient.getJson('/', callback)
  }

  _addProduct(item){
    const callback = this._buildCallback('added.data')
    this.apiClient.getJson('/add/' + item + '/5', callback)
  }
}

export default Service