import Button from './Button.js'
import Service from './Service.js'
import Component from './Component.js'
import Bus from './Bus.js'
import ApiClient from './ApiClient.js'


window.customElements.define('my-button', Button)

const apiClient = new ApiClient()
const bus = new Bus()

new Service(bus, apiClient)

const component = new Component(bus)
component.initialize()