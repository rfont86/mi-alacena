class Component {

  constructor(bus) {
    this.bus = bus
    
    this.listComponent = document.querySelector('#list-component')
    this.button = this.listComponent._shadowRoot.querySelector('button')
    this.input = this.listComponent._shadowRoot.querySelector('input')

    this._subscribe()
  }

  _subscribe(){
    this.bus.subscribe('obtained.data', this.datos.bind(this))
  }

  initialize(){
    this.button.addEventListener('click', () => { this.addItem() })

    this.bus.publish('obtain.data')
  }

  addItem() {
    const product = this.input.value
    this.listComponent.setItems([product])
    this.input.value = ""

    this.bus.publish('add.data', product)
  }

  datos(data) { 
    this.initialItems = Object.keys(data)
    this.listComponent.setItems(this.initialItems)
  }
}

export default Component