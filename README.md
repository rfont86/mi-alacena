 # ALACENA KILLERS by DEVSCOLA

  ## System requeriments

  - Firefox, última versión 73.0.1
  - Docker-compose

  ## How to run the application

  - Console use command  `docker-compose up --build`
  - APP in `localhost:8080`
  - API in `localhost:8081`

  ## How to run test 

  - Console with Jest use command,  `npm run test`.
  - Console with Cypress use command, `docker-compose run -T e2e npm run cypress`.
  - With docker up use command,  `run-test.sh`.

  ## Deploying demo

  Currently, we have a  deploy  application for demo purposes, you can check it out at:

  -[APP]  https://alacenakillers-app.herokuapp.com
  -[API]  https://alacenakillers-api.herokuapp.com
