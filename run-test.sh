#/bin/sh

docker-compose exec -T api npm test
docker-compose exec -T app npm test
docker-compose run e2e npm run cypress