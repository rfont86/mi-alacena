const express = require('express')
const api = express()
const cors = require('cors')

const port = process.env.PORT || 8081

// const {AddItem} = require('./AddItem.js')
// const addItem = new AddItem()

const fs = require('fs')
const readData = fs.readFileSync('list.json')
const list = JSON.parse(readData)

api.use(cors())

api.get('/', function (req, res) {

  res.send(list)
})

api.get('/add/:item/:quantity', (request, response) => {
  let data = request.params
  let item = data.item

  let quantity = Number(data.quantity)

  list[item] = quantity
  let payload = JSON.stringify(list, null, 2)
  fs.writeFile('list.json', payload, finished)

  function finished(err) {
    console.log('all set');
  }

  response.send(list)

  // addItem.add(request.params)
})

api.listen(port, () => console.log(`Almuerzo en el Berlanga!!  ${port}!`))